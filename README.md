Decoratorator is a Python "obfuscator" that transforms any Python 3 code into an import and three function definitions.
It works by generating the source code character by character using function decorators.

## Usage

Decoratorator uses a lookup table to generate the resulting code. The lookup table is built on the first use (or if the `-r` option is provided) and saved to `decolookup.dat`. This can take some time on the default settings. When building, the `-d`, `-m` and `-M` options can be used to optimize performance and character set size.

Decoratorator can either print or execute the given code. If printing is desired, the `-p` option should be provided.

By default, the code is taken from the command line. A file can be specified by providing the `-f` option.

## Example: Hello World! (print mode)

    import math
    @bytes.decode
    @bytes
    @next
    @iter
    @range
    @bool
    def f(): pass
    @list
    @chr
    @math.ceil
    @math.e.__rsub__
    @math.tan
    @math.tan
    @bool
    def g(): pass
    @print
    @f.join
    @g.__add__
    @list
    @chr
    @math.floor
    @math.cosh
    @math.e.__mul__
    @math.cosh
    @bool
    @g.__iadd__
    @chr
    @math.floor
    @math.degrees
    @math.log10
    @math.degrees
    @bool
    @g.__iadd__
    @chr
    @math.ceil
    @math.exp
    @math.exp
    @math.cosh
    @bool
    @g.__iadd__
    @chr
    @math.floor
    @math.degrees
    @math.e.__floordiv__
    @bool
    @g.__iadd__
    @chr
    @math.floor
    @math.exp
    @math.e.__sub__
    @int.__invert__
    @bool
    @g.__iadd__
    @chr
    @math.floor
    @math.e.__rsub__
    @math.degrees
    @math.acos
    @bool
    @g.append
    @chr
    @math.ceil
    @math.cosh
    @math.pi.__add__
    @bool
    @g.__iadd__
    @chr
    @math.floor
    @math.exp
    @math.e.__sub__
    @int.__invert__
    @bool
    @g.__iadd__
    @chr
    @math.ceil
    @math.exp
    @math.exp
    @math.cosh
    @bool
    @g.__iadd__
    @chr
    @math.ceil
    @math.exp
    @math.exp
    @math.cosh
    @bool
    @g.__iadd__
    @chr
    @math.ceil
    @math.degrees
    @math.log10
    @math.degrees
    @bool
    def g(): pass