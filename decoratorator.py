import argparse, itertools, math, os, pickle, sys
parser = argparse.ArgumentParser()
parser.add_argument("-p", action="store_true", help="print instead of executing")
parser.add_argument("-f", action="store_true", help="take code from a file")
parser.add_argument("-d", type=int, metavar="depth", default=4, help="the lookup table recursion depth")
parser.add_argument("-m", type=int, metavar="max", default=10**5, help="the maximum lookup table value")
parser.add_argument("-M", type=int, metavar="max", default=50, help="the maximum factorial input")
parser.add_argument("-r", action="store_true", help="force rebuilding of lookup tables")
parser.add_argument("code", help="the file or code to convert")
args = parser.parse_args()

if args.f:
    try:
        with open(args.code) as file:
            code = file.read()
    except:
        print("Couldn't read file.", file = sys.stderr)
else:
    code = args.code

lookup_other = None
if not args.r and os.path.exists("decolookup.dat"):
    print("Reading lookup tables.", file = sys.stderr)
    try:
        with open("decolookup.dat", "rb") as file:
            lookup_first = pickle.load(file)
            lookup_other = pickle.load(file)
    except:
        print("Couldn't read lookup tables.", file = sys.stderr)
if not lookup_other:
    print("Building lookup tables.", end = "", file = sys.stderr)
    funcs = ["math.abs", "math.acos", "math.acosh", "math.asin", "math.asinh", "math.atan", "math.ceil", "math.cos", "math.cosh", "math.degrees",
             "math.erf", "math.erfc", "math.exp", "math.expm1", "math.factorial", "math.floor", "math.gamma", "math.isfinite", "math.isnan",
             "math.lgamma", "math.log", "math.log10", "math.log1p", "math.log2", "math.radians", "math.round", "math.sin", "math.sinh",
             "math.sqrt", "math.tan", "math.tanh", "math.trunc"]
    for method in ["add", "sub", "mul", "truediv", "floordiv", "mod"]:
        for constant in ["math.e", "math.pi"]:
            funcs.append(constant + ".__" + method + "__")
            funcs.append(constant + ".__r" + method + "__")
    funccount = len(funcs)
    count = 0
    total = 16 * funccount ** args.d
    lookup_first = {}
    lookup_other = {}
    def addvalue(value, path, lookup):
        if value not in lookup or len(path) < len(lookup[value]) or (len(path) == len(lookup[value]) and len(set(path)) < len(set(lookup[value]))):
            lookup[value] = path[:]
    def build(value, path, lookup, depth = args.d):
        global count
        if isinstance(value, int):
            addvalue(value, path, lookup)
        else:
            path.append("trunc")
            try:
                addvalue(trunc(value), path, lookup)
                path[-1] = "floor"
                addvalue(floor(value), path, lookup)
                path[-1] = "ceil"
                addvalue(ceil(value), path, lookup)
            except KeyboardInterrupt:
                raise
            except:
                pass
            path.pop()
        if depth > 0:
            if abs(value) <= args.m:
                for func in funcs:
                    path.append(func)
                    try:
                        assert func is not "math.factorial" or value <= args.M
                        build(eval(func)(value), path, lookup, depth = depth - 1)
                    except KeyboardInterrupt:
                        raise
                    except:
                        count += funccount ** (depth - 1)
                    path.pop()
            else:
                count += funccount ** depth
        if depth == 0:
            count += 1
        if depth == 2:
            progress = 100 * count / total
            print("\rBuilding lookup tables: %.1f%%" % progress, end = "", file = sys.stderr)
    build(-2, ["bool", "int.__invert__"], lookup_first)
    build(-1, ["bool", "int.__neg__"], lookup_first)
    build(1, ["bool"], lookup_first)
    build(2, ["bool", "int.__invert__", "str", "len"], lookup_first)
    build(4, ["bool", "str", "len"], lookup_first)
    build(18, ["type", "str", "len"], lookup_first)
    build(26, ["str", "len"], lookup_first)
    build(-2, ["g.__iadd__", "bool", "int.__invert__"], lookup_other)
    build(-1, ["g.append", "bool", "int.__invert__"], lookup_other)
    build(0, ["g.append", "bool"], lookup_other)
    build(1, ["g.__iadd__", "bool"], lookup_other)
    build(2, ["g.append", "bool", "int.__invert__", "str", "len"], lookup_other)
    build(4, ["g.append", "str", "len"], lookup_other)
    build(5, ["g.append", "bool", "str", "len"], lookup_other)
    build(14, ["g.__iadd__", "type", "str", "len"], lookup_other)
    build(18, ["g.append", "type", "str", "len"], lookup_other)
    print("\rBuilt lookup tables.          ", file = sys.stderr)
    print("Saving lookup tables.", file = sys.stderr)
    try:
        with open("decolookup.dat", "wb") as file:
            pickle.dump(lookup_first, file)
            pickle.dump(lookup_other, file)
    except:
        print("Couldn't save lookup tables.", file = sys.stderr)

print("""import math
@bytes.decode
@bytes
@next
@iter
@range
@bool
def f(): pass""")
for position, char in enumerate(itertools.chain(code[:1], code[:0:-1])):
    if position == 0:
        print("@list")
    elif position == 1:
        print("@print" if args.p else "@exec")
        print("""@f.join
@g.__add__
@list""")
    lookup = lookup_first if position in [0, len(code) - 1] else lookup_other
    charcode = ord(char)
    if charcode in lookup:
        method = lookup[charcode]
    else:
        print("Impossible character: " + char, file = sys.stderr)
        sys.exit(0)
    print("@chr")
    for row in method[::-1]:
        print("@" + row)
    if position in [0, len(code) - 1]:
        print("def g(): pass")
